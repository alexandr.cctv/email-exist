## Using and information
**npm install email-exist** and enjoy !)

##Example 

```js
var verifier = require('email-exist');
verifier.verify( 'test@test.com', function( err, info ){
  if( err ) {
    console.log(err);
  }else{
    console.log( "Success: " + info.success );
    console.log( "Info: " + info.info );
    console.log( "Response from smtp: " + info.response );
  }
});
```
**return** 

```js
{
  success: boolean
  info: string
  addr: the address being verified
  response: string
}
```

it is fork fom email-verify package.
I add tries to connect other ports and smtp response log