let assert = require('assert');

describe('email-exist', function () {
    this.timeout(20000);

    let verifier = require('../index.js');

    describe('#verify()', function () {
        it('existing email: should respond with an object where success is true', function (done) {
            verifier.verify('office@worldsmi.com', {loggerStatus:true},function (err, info) {
                assert(info.success);
                done();
            });
        });
        it('non-existing email: should respond with an object where success is false', function(done){
            verifier.verify('officetest@worldsmi.com', function (err, info) {
                assert(!info.success);
                done();
            });
        });
    });
});
